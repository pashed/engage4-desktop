import React, {useEffect, useState} from 'react'
import Logout from '../Login/logout'
import axios from 'axios';
import {withRouter} from "react-router-dom";

const handleLogout = async props => {
  props.onLogout();
  props.history.push("/login");
};

function Profile(props) {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [jobTitle, setJobTitle] = useState('');
  const [phone, setPhone] = useState('');
  const [avatar, setAvatar] = useState('');

  useEffect(() => {
    async function fetchMe() {
      const {data} = await axios({
        method: 'get',
        url: 'http://localhost:3030/v1/content/user/me',
        headers: {
          'Authorization': `Bearer ${props.token}`
        }
      }).catch(e => console.log(e));
      console.log("DATA", data);
      setFirstName(data.me.first_name);
      setLastName(data.me.last_name);
      setJobTitle(data.me.job_title);
      setPhone(data.me.phone_number);
      const avatar = sessionStorage.getItem('image1');
      // console.log(avatar);
      if (avatar) setAvatar(avatar);
      else {

        axios({
          url: `http://localhost:3030/v1/content/image/${data.me.image_id}/thumbnail`,
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${props.token}`
          },
          responseType: 'arraybuffer'
        }).then(res => {
          let data = new Uint8Array(res.data);
          let raw = String.fromCharCode.apply(null, data);
          let base64 = btoa(raw);
          let src = "data:image;base64," + base64;
          sessionStorage.setItem('image1', src);
          setAvatar(src);
        }).catch(e => {
          console.log(e);
        });
      }
    }

    fetchMe();
  }, [props.token]);

  const processAvatar = (event) => {
    console.log(event);
  };

  return (
    <div>
      <h2>Edit</h2>
      <form action="" onSubmit={(event) => {
        event.preventDefault()
      }}>
        <img placeholder={'Profile Picture'} alt={'Profile'} src={avatar}/>
        <input type="file" name={'image'} onChange={processAvatar}/>
        <label htmlFor="first_name">
          First Name
          <input type="text" name={'first_name'} value={firstName} onChange={setFirstName}/>
        </label>
        <label htmlFor="last_name">
          Last Name
          <input type="text" name={'last_name'} value={lastName} onChange={setLastName}/>
        </label>
        <label htmlFor="job_title">
          Job Title
          <input type="text" name={'job_title'} value={jobTitle} onChange={setJobTitle}/>
        </label>
        <label htmlFor="phone_number">
          Phone Name
          <input type="text" name={'phone_number'} value={phone} onChange={setPhone}/>
        </label>
        <input type="submit"/>
        <input type="submit" value="Save"/>
      </form>
      <Logout handleLogout={() => handleLogout(props)}/>
    </div>
  )
}

// eslint-disable-next-line no-func-assign
export default Profile = withRouter(Profile)