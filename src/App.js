import React from 'react';
import './App.css';
import {
  Switch,
  Route,
  NavLink,
  Redirect,
} from "react-router-dom";
import Feed from './Feed/feed'
import Engage from './Engage/engage'
import Activity from './Activity/activity'
import AppNotification from './Notivications/notification'
import Search from './Search/search'
import Profile from './Profile/profile'
import LoginComponent from "./Login/LoginComponent";
import AuthWithMagicToken from './Login/MagicLoginComponent';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      accessToken: localStorage.getItem('accessToken'),
      posts: null,
    };
  }

  setToken = token => {
    localStorage.setItem('accessToken', token);
    this.setState({accessToken: token});
  };

  onLogout = () => {
    localStorage.removeItem('accessToken');
    this.setState({accessToken: null})
  };

  render() {
    return (
      <Switch>
        <Route exact path={'/login'} component={() => {
          return this.state.accessToken ? <Redirect to={'/'}/> : <LoginComponent setToken={this.setToken}/>
        }}/>
        <Route exact path={'/token_auth/:token'}
               component={({match}) => {
                 return this.state.accessToken ?
                   <Redirect to={'/'}/> :
                   <AuthWithMagicToken setToken={this.setToken} magicToken={match.params.token}/>
               }}/>
        <ProtectedRoutes>
          <nav>
            <ul>
              <li>
                <NavLink to="/feed">Feeds</NavLink>
              </li>
              <li>
                <NavLink to="/engage">Engage</NavLink>
              </li>
              <li>
                <NavLink to="/activity">Activity</NavLink>
              </li>
              <li>
                <NavLink to="/notifications">Notifications</NavLink>
              </li>
              <li>
                <NavLink to="/search">Search Org</NavLink>
              </li>
              <li>
                <NavLink to="/profile">Profile</NavLink>
              </li>
            </ul>
          </nav>
          <Route exact path="/" render={() => (
            <Redirect to="/feed"/>
          )}/>
          <Route path="/feed" component={() => (<Feed posts={this.state.posts} token={this.state.accessToken}/>)}/>
          <Route path="/engage" component={Engage}/>
          <Route path="/activity" component={Activity}/>
          <Route path="/notifications" component={AppNotification}/>
          <Route path="/search" component={Search}/>
          <Route path="/profile"
                 component={() => (<Profile onLogout={this.onLogout} token={this.state.accessToken}/>)}/>
        </ProtectedRoutes>
      </Switch>
    )
  }
}

function ProtectedRoutes(props) {
  if (localStorage.getItem('accessToken')) return props.children;
  return (
    <Redirect to={'/login'}/>
  );
}

function ErrorPopUp(props) {
  return (
    <div>This is popUp with Error
      {props.error}
    </div>
  )
}

export default App;
