import React, {useEffect, useState} from 'react'
import axios from 'axios'


export default function Feed(props) {
  const [limit, setLimit] = useState(15);
  const [page, setPage] = useState(1);
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    async function fetch() {
      const {data} = await axios({
        method: 'get',
        url: 'http://localhost:3030/v1/content/feed',
        headers: {
          'Authorization': `Bearer ${props.token}`,
          'Accept': 'application/json; charset=utf-8'
        },
        params: {
          limit: 10,
          page: 1
        }
      }).catch(e => console.log(e));
      console.log("DATA", data);
      setPosts(data.posts)
    };

    fetch();
  }, []);

  return (
    <div>
      <Post data={posts}/>
    </div>
  )
}

function Post(props) {
  console.log(props);

  const posts = props.data.map(post => (
    <div key={post.id}>
      <span> {post.postable_type}</span>
      <span> {post.postable_id}</span>
      <span> {post.comments_counter}</span>
      <span> {post.likes_counter}</span>
    </div>
  ));
  return (
    posts
  )
}