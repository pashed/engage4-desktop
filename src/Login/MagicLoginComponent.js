import React, {useEffect} from 'react'
import axios from 'axios';
import jwtDecode from "jwt-decode";
import {withRouter} from "react-router-dom";

function AuthWithMagicToken(props) {

  useEffect(() => {
    let decodedJwt = jwtDecode(Buffer.from(props.magicToken, "base64").toString("utf8"));
    console.log(decodedJwt);
    axios.post(
      'http://localhost:3030/v1/authentication/token_authentication',
      {
        email_address: decodedJwt.email,
        token: props.magicToken,
        device_key: decodedJwt.device_key
      }
    )
      .then(response => {
        if (response.status === 200 && response.data.success === true) {
          props.setToken(response.data.key);
          this.props.history.push("/");
        }
      })
      .catch((error) => {
        if (error.response && [401, 403, 404].includes(error.response.status)) {
          alert(error.response.data.message); //TODO remove it !!! for debug only
        }
      })
  });

  return (
    <div>m t l </div>
  )
}

// eslint-disable-next-line no-func-assign
export default AuthWithMagicToken = withRouter(AuthWithMagicToken);