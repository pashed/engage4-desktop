import React from 'react';
import axios from 'axios';
import { withRouter } from "react-router-dom";

class LoginComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      responseStatus: null,
      accessToken: props.accessToken,
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmit = async event => {
    event.preventDefault();
    if(this.state.loginToken) {
      axios.post(
        'http://localhost:3030/v1/authentication/password_authentication',
        {
          email_address: this.state.email,
          password: this.state.password,
          token: this.state.loginToken,
          device_key: 123123
        }
      )
        .then(response => {
          console.log(response.data);
          if (response.status === 200 && response.data.success === true && response.data.auth_type !== "MAGIC_LINK") {
            console.log(response.status);
            this.props.setToken(response.data.key);
            this.props.history.push("/");
          }
        })
        .catch(error => {
          console.log(error);
        });
    } else {
      axios.post(
        'http://localhost:3030/v1/authentication/request_authentication',
        {
          email_address: this.state.email,
          device_key: 123123
        }
      )
        .then(response => {
          console.log(response.data);
          if (response.data.success === true && response.data.auth_type !== "MAGIC_LINK") {
            console.log(response.data.token);
            this.setState({loginToken: response.data.token});
          }
          if (response.data.success === true && response.data.auth_type === "MAGIC_LINK") {
            console.log(response.data.token);
            alert('MagicLink has been sent to your email');

          }
        })
        .catch(error => {
          console.log(error);
        });
    }
  };

  render() {
    const isPasswordUser = this.state.loginToken;
    let password;
    if(isPasswordUser) {
      password =<label>
        password:
        <input type="password" name="password" value={this.state.password} onChange={this.handleChange}/>
      </label>
    }
    return (
      <div>
        login
        <form onSubmit={this.handleSubmit}>
          <label>
            Name:
            <input type="email" name="email" value={this.state.email} onChange={this.handleChange}/>
          </label>
          {password}
          <input type="submit" value="Login"/>
        </form>
      </div>
    )
  }
}

export default LoginComponent = withRouter(LoginComponent);